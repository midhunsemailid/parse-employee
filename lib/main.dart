import 'package:flutter/material.dart';
import 'package:parse_employee/app/employee_app.dart';

import 'data/utils/parse_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await ParseService.initialize();
  runApp(const EmployeeApp());
}

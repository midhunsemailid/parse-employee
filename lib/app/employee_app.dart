/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-03-14 07:52:14

*/
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:parse_employee/bloc/employee_bloc.dart';
import 'package:parse_employee/presentation/employees/employees_page.dart';

class EmployeeApp extends StatelessWidget {
  const EmployeeApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => EmployeeBloc(),
      child: MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.brown,
        ),
        home: const EmployeesPage(),
      ),
    );
  }
}

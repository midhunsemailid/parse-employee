import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:parse_employee/data/model/entity/employee.dart';
import 'package:parse_employee/data/model/processing_models/core/data_error.dart';
import 'package:parse_employee/data/repositories/employee_repository.dart';

part 'employee_event.dart';
part 'employee_state.dart';

enum EmployeeStateStatus { inProgress, failure, success }

class EmployeeBloc extends Bloc<EmployeeEvent, EmployeeState> {
  final EmployeeRepository _employeeRepository;
  EmployeeBloc({EmployeeRepository? employeeRepository})
      : _employeeRepository = employeeRepository ?? EmployeeRepositoryImp(),
        super(EmployeeInitial()) {
    on<EmployeeLoadEvent>((event, emit) async {
      emit(EmployeeLoadState.inProgress());
      final result = await _employeeRepository.getAllEmployees();
      if (result.isSuccess) {
        emit(EmployeeLoadState.success(employees: result.data ?? []));
      } else {
        emit(
          EmployeeLoadState.failure(failure: result.error as ParseException),
        );
      }
    });

    on<EmployeeAddEvent>((event, emit) async {
      emit(EmployeeAddState.inProgress());
      final result = await _employeeRepository.addEmployee(event.employee);
      if (result.isSuccess) {
        emit(EmployeeAddState.success());
      } else {
        emit(
          EmployeeAddState.failure(failure: result.error as ParseException),
        );
      }
    });
  }
}

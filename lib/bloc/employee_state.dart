part of 'employee_bloc.dart';

@immutable
abstract class EmployeeState {}

class EmployeeInitial extends EmployeeState {}

class EmployeeLoadState extends EmployeeState {
  final EmployeeStateStatus status;
  EmployeeLoadState._(this.status, {this.employees, this.failure});
  EmployeeLoadState.inProgress() : this._(EmployeeStateStatus.inProgress);
  EmployeeLoadState.success({required List<Employee> employees})
      : this._(EmployeeStateStatus.success, employees: employees);
  EmployeeLoadState.failure({required ParseException failure})
      : this._(EmployeeStateStatus.failure, failure: failure);
  final List<Employee>? employees;
  final ParseException? failure;
}

class EmployeeAddState extends EmployeeState {
  final EmployeeStateStatus status;
  EmployeeAddState._(this.status, {this.failure});
  EmployeeAddState.inProgress() : this._(EmployeeStateStatus.inProgress);
  EmployeeAddState.success() : this._(EmployeeStateStatus.success);
  EmployeeAddState.failure({required ParseException failure})
      : this._(EmployeeStateStatus.failure, failure: failure);
  final ParseException? failure;
}

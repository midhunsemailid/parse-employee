part of 'employee_bloc.dart';

@immutable
abstract class EmployeeEvent {}

class EmployeeLoadEvent extends EmployeeEvent {}

class EmployeeAddEvent extends EmployeeEvent {
  EmployeeAddEvent(this.employee);
  final Employee employee;
}

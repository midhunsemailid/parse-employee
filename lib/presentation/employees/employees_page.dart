/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-03-14 07:55:56

*/

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:parse_employee/bloc/employee_bloc.dart';
import 'package:parse_employee/data/model/entity/employee.dart';
import 'package:parse_employee/presentation/add_employee/app_employee_page.dart';

part './widgets/employee_empty_view.dart';
part './widgets/employee_listing_view.dart';
part './widgets/employee_load_error_view.dart';
part './widgets/employee_loading_view.dart';

class EmployeesPage extends StatefulWidget {
  const EmployeesPage({Key? key}) : super(key: key);

  @override
  State<EmployeesPage> createState() => _EmployeesPageState();
}

class _EmployeesPageState extends State<EmployeesPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<EmployeeBloc>(context).add(EmployeeLoadEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Employee List'),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            final result = await Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => const AddEmployeePage()),
            );
            if (result != null) {
              if (result is bool) {
                BlocProvider.of<EmployeeBloc>(context).add(EmployeeLoadEvent());
              }
            }
          },
          child: const Icon(
            Icons.add,
            size: 24.0,
          ),
        ),
        body: Container(
          alignment: Alignment.center,
          child: BlocBuilder<EmployeeBloc, EmployeeState>(
            bloc: BlocProvider.of<EmployeeBloc>(context),
            buildWhen: (previous, current) => current is EmployeeLoadState,
            builder: (context, state) {
              if (state is EmployeeLoadState) {
                switch (state.status) {
                  case EmployeeStateStatus.inProgress:
                    return const EmployeeLoadingView();
                  case EmployeeStateStatus.failure:
                    final errorText = state.failure?.description ??
                        'Failed to load employees';
                    return EmployeeLoadErrorView(errorText: errorText);
                  case EmployeeStateStatus.success:
                    final employees = state.employees ?? [];
                    if (employees.isEmpty) {
                      return const EmployeeEmptyView();
                    } else {
                      return EmployeeListingView(employees: employees);
                    }
                }
              }
              return Container(
                alignment: Alignment.center,
                child: const Text('Expected error occurred'),
              );
            },
          ),
        ));
  }
}

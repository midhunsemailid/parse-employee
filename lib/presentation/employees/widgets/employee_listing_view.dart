/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-03-14 15:00:20

*/
part of '../employees_page.dart';

class EmployeeListingView extends StatelessWidget {
  const EmployeeListingView({
    Key? key,
    required this.employees,
  }) : super(key: key);

  final List<Employee> employees;

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () {
        final itemsBloc = BlocProvider.of<EmployeeBloc>(context)
          ..add(EmployeeLoadEvent());
        return itemsBloc.stream.firstWhere((e) => e is! EmployeeLoadState);
      },
      child: ListView.separated(
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(employees[index].name),
              subtitle: Text(employees[index].email),
            );
          },
          separatorBuilder: (context, index) {
            return const Divider(
              thickness: 1.0,
            );
          },
          itemCount: employees.length),
    );
  }
}

/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-03-14 14:56:43

*/
part of '../employees_page.dart';

class EmployeeLoadingView extends StatelessWidget {
  const EmployeeLoadingView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: const [
        SizedBox(
          child: CircularProgressIndicator(
            strokeWidth: 1.0,
          ),
          height: 24.0,
          width: 24.0,
        ),
        Padding(
          padding: EdgeInsets.all(8.0),
          child: Text('loading employee\'s'),
        )
      ],
    );
  }
}

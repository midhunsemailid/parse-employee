/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-03-14 14:59:37

*/
part of '../employees_page.dart';

class EmployeeLoadErrorView extends StatelessWidget {
  const EmployeeLoadErrorView({
    Key? key,
    required this.errorText,
  }) : super(key: key);

  final String errorText;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Icon(
          Icons.error,
          size: 44.0,
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(errorText),
        )
      ],
    );
  }
}

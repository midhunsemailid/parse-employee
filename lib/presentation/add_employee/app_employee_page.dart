/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-03-14 08:54:34

*/

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:parse_employee/bloc/employee_bloc.dart';
import 'package:parse_employee/data/model/entity/employee.dart';
import 'widgets/error_pop_up.dart';
import 'widgets/loading_indicator_pop_up.dart';

final emailRegExp = RegExp(
    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

class AddEmployeePage extends StatefulWidget {
  const AddEmployeePage({Key? key}) : super(key: key);

  @override
  State<AddEmployeePage> createState() => _AddEmployeePageState();
}

class _AddEmployeePageState extends State<AddEmployeePage> {
  final _formKey = GlobalKey<FormState>();
  BuildContext? _loaderDisMissContext;

  String? _email;
  String? _name;
  String? _id;

  Widget _addFloatingActionButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: FloatingActionButton.extended(
        onPressed: () {
          FocusScope.of(context).unfocus();
          if (_formKey.currentState!.validate()) {
            BlocProvider.of<EmployeeBloc>(context).add(EmployeeAddEvent(
              Employee(id: _id!, email: _email!, name: _name!),
            ));
          }
        },
        label: Row(
          children: const [
            Icon(Icons.add),
            Text('Add'),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Employee'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: _addFloatingActionButton(context),
      body: BlocListener<EmployeeBloc, EmployeeState>(
        listenWhen: (previous, current) => current is EmployeeAddState,
        listener: (context, state) async {
          if (state is EmployeeAddState) {
            if (_loaderDisMissContext != null) {
              Navigator.of(_loaderDisMissContext!).pop();
              _loaderDisMissContext = null;
            }
            switch (state.status) {
              case EmployeeStateStatus.inProgress:
                _loaderDisMissContext =
                    await LoadingIndicatorPopUp.show(context);

                break;
              case EmployeeStateStatus.failure:
                FailurePopUp.show(context,
                    message: state.failure?.description ??
                        'Unexpected error occurred');
                break;
              case EmployeeStateStatus.success:
                Navigator.of(context).pop(true);
                break;
            }
          }
        },
        child: SafeArea(
          child: Form(
            key: _formKey,
            child: ListView(
              padding: const EdgeInsets.all(16.0),
              children: [
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  decoration: const InputDecoration(
                      label: Text('Name'),
                      helperText: '*required (First and last name).'),
                  onChanged: (value) => _name = value,
                  validator: (value) {
                    if (value == null) {
                      return 'This field can \'t be empty';
                    }
                    if (value.isEmpty) {
                      return 'This field can \'t be empty';
                    }
                    if (value.length < 3) {
                      return 'Name must contain at least 3 letters';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  decoration: const InputDecoration(
                      label: Text('Email'), helperText: '*required'),
                  onChanged: (value) => _email = value,
                  validator: (value) {
                    if (value == null) {
                      return 'This field can \'t be empty';
                    }
                    if (value.isEmpty) {
                      return 'This field can \'t be empty';
                    }
                    if (!emailRegExp.hasMatch(value)) {
                      return 'Not valid email';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  decoration: const InputDecoration(
                    label: Text('Employee Id'),
                    helperText: '*required.',
                  ),
                  onChanged: (value) => _id = value,
                  validator: (value) {
                    if (value == null) {
                      return 'This field can \'t be empty';
                    }
                    if (value.isEmpty) {
                      return 'This field can \'t be empty';
                    }
                    if (value.length < 4) {
                      return 'id must contain at least 4 letters';
                    }
                    return null;
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

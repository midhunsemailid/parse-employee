/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-03-14 10:36:10

*/

import 'dart:async';

import 'package:flutter/material.dart';

class LoadingIndicatorPopUp extends StatelessWidget {
  const LoadingIndicatorPopUp({Key? key}) : super(key: key);

  static Future<BuildContext> show(BuildContext context) {
    final dialogContextCompleter = Completer<BuildContext>();
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          if (!dialogContextCompleter.isCompleted) {
            dialogContextCompleter.complete(context);
          }
          return const LoadingIndicatorPopUp();
        });
    return dialogContextCompleter.future;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: const [
              SizedBox(
                height: 8.0,
              ),
              SizedBox(
                child: CircularProgressIndicator(
                  strokeWidth: 1.0,
                ),
                height: 30.0,
                width: 30.0,
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text('processing...'),
              )
            ],
          ),
        ),
      ),
    );
  }
}

/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-03-14 10:41:11

*/

import 'package:flutter/material.dart';

class FailurePopUp extends StatelessWidget {
  const FailurePopUp({Key? key, required this.message}) : super(key: key);
  final String message;

  static Future<void> show(BuildContext context, {required String message}) {
    return showDialog(
      context: context,
      builder: (context) => FailurePopUp(
        message: message,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Failed To Add Employee',
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1
                      ?.copyWith(fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(message),
              ),
              const SizedBox(
                height: 16.0,
              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('Cancel'))
            ],
          ),
        ),
      ),
    );
  }
}

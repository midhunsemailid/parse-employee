/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-03-14 07:07:35

*/

import 'package:parse_employee/data/model/entity/employee.dart';
import 'package:parse_employee/data/model/processing_models/core/data_error.dart';
import 'package:parse_employee/data/model/processing_models/core/data_result.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

abstract class EmployeeRepository {
  Future<DataResult<List<Employee>>> getAllEmployees();
  Future<DataResult<bool>> addEmployee(Employee employee);
}

class EmployeeRepositoryImp extends EmployeeRepository {
  @override
  Future<DataResult<bool>> addEmployee(Employee employee) async {
    final response = await employee.toParse().save();
    if (response.success) {
      return DataResult.success(true);
    } else {
      return DataResult.failure(ParseException(
          statusCode: response.error?.code ?? -1,
          description: response.error?.message ?? ''));
    }
  }

  @override
  Future<DataResult<List<Employee>>> getAllEmployees() async {
    try {
      final response = await Employee.parseEmployee().getAll();
      if (response.success) {
        final employees = response.results
                ?.cast<ParseObject>()
                .map((parseObject) => Employee.fromParse(parseObject))
                .toList() ??
            [];
        return DataResult.success(employees);
      } else {
        return DataResult.failure(ParseException(
            statusCode: response.error?.code ?? -1,
            description: response.error?.message ?? ''));
      }
    } catch (e) {
      return DataResult.failure(ParseException(
          statusCode: -1, description: 'Undefined error occurred'));
    }
  }
}

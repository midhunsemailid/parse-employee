/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-03-14 07:03:30

*/

import 'package:equatable/equatable.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

class Employee extends Equatable {
  const Employee({
    required this.email,
    required this.id,
    required this.name,
  });
  final String name;
  final String email;
  final String id;
  static ParseObject parseEmployee() => ParseObject('Employee');

  factory Employee.fromParse(ParseObject parse) => Employee(
        id: parse.get<String>('emp_id') ?? '',
        name: parse.get<String>('name') ?? '',
        email: parse.get<String>('email') ?? '',
      );

  ParseObject toParse() {
    return parseEmployee()
      ..set('emp_id', id)
      ..set('email', email)
      ..set('name', name);
  }

  @override
  List<Object?> get props => [id, name, email];
}

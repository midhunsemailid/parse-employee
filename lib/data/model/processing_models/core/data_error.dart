/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-03-08 18:15:41

*/
import 'package:equatable/equatable.dart';

abstract class DataError extends Equatable implements Exception {
  @override
  String toString() => '$runtimeType Exception';
  @override
  List<Object> get props => [];
}

class ParseException implements DataError {
  ParseException({required this.statusCode, required this.description});
  final int statusCode;
  final String description;
  @override
  String toString() => '$runtimeType ParseException';

  @override
  List<Object> get props => [statusCode];

  @override
  bool? get stringify => true;
}

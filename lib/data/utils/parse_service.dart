/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-03-14 07:45:33

*/

import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

class ParseService {
  static Future<void> initialize() async {
    const keyApplicationId = 'v4GaP4iu1hNONAMpjfFx4IAvNoTrPAodvIkdSaBI';
    const keyClientKey = 'dmDZqXgeJXNymXaXkhRX4UamaTWOKKbIl3JS3psB';
    const keyParseServerUrl = 'https://parseapi.back4app.com';
    await Parse().initialize(keyApplicationId, keyParseServerUrl,
        clientKey: keyClientKey, autoSendSessionId: true);
    return;
  }
}
